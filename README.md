# mrtol

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

# aurora-lunar是集成lunar进行开发的Uniapp插件
# [lunar](https://6tail.cn/calendar/api.html)是一个支持阳历、阴历、佛历和道历的日历工具库，它开源免费，有多种开发语言的版本，
# 不依赖第三方，支持阳历、阴历、佛历、道历、儒略日的相互转换，
# 还支持星座、干支、生肖、节气、节日、彭祖百忌、每日宜忌、吉神宜趋、凶煞宜忌、
# 吉神方位、冲煞、纳音、星宿、八字、五行、十神、建除十二值星、青龙名堂等十二神、黄道日及吉凶等。仅供参考，切勿迷信
# 使用说明
# 1.下载插件到指定项目
# 2.在使用的页面引入lunar.min.js
# 
```javascript
import {
	Solar,
	Lunar,
	HolidayUtil,
	SolarWeek
} from '../../uni_modules/aurora-lunar/components/aurora-lunar/lunar.min.js';
```
# 3.lunar官方文档：[lunar官方文档](https://6tail.cn/calendar/api.html)
# 4.阳历的实例化有以下几种方式：
Solar.fromYmd(year, month, day)
指定阳历年(数字)、阳历月(数字)、阳历日(数字)生成阳历对象。注意月份为1到12。

Solar.fromYmdHms(year, month, day, hour, minute, second)
指定阳历年(数字)、阳历月(数字)、阳历日(数字)、阳历小时(数字)、阳历分钟(数字)、阳历秒钟(数字)生成阳历对象。注意月份为1到12。

Solar.fromDate(date)
指定日期(Date)生成阳历对象

Solar.fromJulianDay(julianDay)
指定儒略日(小数)生成阳历对象

Solar.fromBaZi(yearGanZhi, monthGanZhi, dayGanZhi, timeGanZhi, sect, baseYear)
通过八字年柱、月柱、日柱、时柱获取匹配的阳历列表。sect为流派，可选1或2，不传则默认为2。baseYear为起始年份，支持最小传1，不传则默认为1900。该方法可能返回多条满足条件的记录。
# 5.阴历的实例化有以下几种方式：
Lunar.fromYmd(lunarYear, lunarMonth, lunarDay)
指定阴历年(数字)、阴历月(数字)、阴历日(数字)生成阴历对象。注意月份为1到12，闰月为负，即闰2月=-2。

Lunar.fromYmdHms(lunarYear, lunarMonth, lunarDay, hour, minute, second)
指定阴历年(数字)、阴历月(数字)、阴历日(数字)、阳历小时(数字)、阳历分钟(数字)、阳历秒钟(数字)生成阴历对象。注意月份为1到12，闰月为负，即闰2月=-2。

Lunar.fromDate(date)
指定阳历日期(Date)生成阴历对象


[API接口市场](https://www.showapi.com/)